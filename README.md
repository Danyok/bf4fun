# Brainf*ck интерпретатор для Аврора ОС.

Brainf*ck interp for Aurora OS.
Application based on https://www.av13.ru/creative/brainfuck-compiler/ .

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

## Project Structure

The project has a common structure
of an application based on C++ and QML for Aurora OS.

* **[io.github.danyok96.bf4fun.pro](io.github.danyok96.bf4fun.pro)** file
  describes the project structure for the qmake build system.
* **[icons](icons)** directory contains application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[icons](qml/icons)** directory contains the custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[bf4fun.qml](qml/bf4fun.qml)** file
    provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  **[io.github.danyok96.bf4fun.spec](rpm/io.github.danyok96.bf4fun.spec)** file is used by rpmbuild tool.
  It is generated from **[io.github.danyok96.bf4fun.yaml](rpm/io.github.danyok96.bf4fun.yaml)** file.
* **[src](src)** directory contains the C++ source code.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[io.github.danyok96.bf4fun.desktop](io.github.danyok96.bf4fun.desktop)** file
  defines the display and parameters for launching the application.