#include "brainfuck.h"

Brainfuck::Brainfuck(QObject *parent) : QObject(parent)
{

}

QString Brainfuck::interpret(QString str, QString commasStr)
{
//    qDebug()<<"Hi!";
    str = brainfuck(str, commasStr);
//    qDebug()<<str;
    return str;
}

QString Brainfuck::brainfuck(QString inputStr, QString commasStr)
{
    QString outputStr;

    // Defining Main Variables
    brainfuck_t		bf;				// Context
    char			in;				// Input Char
    int				i, j;			// Temp Ints
    stack_p			stack_temp;		// Temp Stack Pointer
    int             commasCouter = 0;
    // Token List
    char			tokens[] = {
        TK_NEXT, TK_PREV, TK_IN, TK_OUT, TK_INC, TK_DEC, TK_STR, TK_END
    };

    // Initializing Context Vars
    bf.commands = 0;
    bf.pos		= 0;
    bf.ptr		= 0;
    bf.stack	= null;

    // Clearing Context Data
    for ( i = 0; i < BRAINFUCKS; i++ ) {
        bf.code[i]	= 0;
        bf.data[i]	= 0;
    }

    // Loading Code
    if ( inputStr.size() != 0) {
        j = 0;
        for(auto curPos = 0; curPos < inputStr.size(); curPos++) {
            in =  QString(inputStr).at(curPos).toLatin1();
            // Do not load more than (big number) commands
            if ( j >= BRAINFUCKS ) break;
            for ( i = 0; i < TK_COUNT; i++ ) {
                // Check if it is command not comment
                if ( in == tokens[i] ) {
                    bf.code[bf.commands] = in;
                    bf.commands ++;
                    j ++;
                    break;
                }
            }
        }
    } else {
        qDebug()<<"Fatal: str is empty!";
    }

    // Start Program Cycle
    while ( bf.pos < bf.commands ) {

        // Check Command Code
        switch ( bf.code[bf.pos] ) {

            // Go To Next Cell ( > )
            case TK_NEXT:
                bf.ptr = (bf.ptr + 1) % BRAINFUCKS;
                break;
            // Go To Prev Cell ( < )
            case TK_PREV:
                bf.ptr --;
                if (bf.ptr < 0) bf.ptr = 0;
                break;

            // Inc Cell Value ( + )
            case TK_INC:
                bf.data[bf.ptr] = (bf.data[bf.ptr] + 1) % 256;
                break;

            // Del Cell Value ( - )
            case TK_DEC:
                bf.data[bf.ptr] --;
                if ( bf.data[bf.ptr] < 0 ) bf.data[bf.ptr] = 0;
                break;

            // Input Char from StdIn ( , )
            case TK_IN:
            // it will be soon...
//                bf.data[bf.ptr] = getchar();
                if(commasCouter < commasStr.size())
                {
                    bf.data[bf.ptr] = QString(commasStr).at(commasCouter).toLatin1();
                    commasCouter++;
                }
                break;

            // Print Char to StrOut ( . )
            case TK_OUT:
//                putchar(bf.data[bf.ptr]);
                outputStr+=bf.data[bf.ptr];
                break;

            // Cycle Start ( [ )
            case TK_STR:
                if ( bf.data[bf.ptr] > 0) {
                    // Start Cycle if *p != 0
                    stack_temp = new stack_t;
//					stack_temp = (stack_p) mc_alloc (sizeof(stack_t));
                    stack_temp->next = bf.stack;
                    stack_temp->command = bf.pos;
                    bf.stack = stack_temp;
                } else {
                    // Go After The ] if *p == 0
                    while ( bf.pos < bf.commands ) {
                        if ( bf.code[bf.pos] == TK_END ) {
                            break;
                        } else {
                            bf.pos ++;
                        }
                    }
                }
                break;

            // Cycle End ( ] )
            case TK_END:
                // Go Back Only If *p != 0
                if ( bf.data[bf.ptr] > 0 ) {
                    if (bf.stack != null) {
                        stack_temp = bf.stack;
                        bf.stack = bf.stack->next;
                        bf.pos = stack_temp->command - 1;
                        delete stack_temp;
                    } else {
                        qDebug()<<"Fatal: stack is empty when ] is called on command"; // bf.pos
                        bf.pos = bf.commands;
                    }
                }
                break;
        }

        // Next Command
        bf.pos ++;

    }

    // Clearing the Stack if Necessary
    while (bf.stack != null) {
        stack_temp = bf.stack;
        bf.stack = bf.stack->next;
        delete stack_temp;
    }

    return outputStr;
}

//void Brainfuck::reciveFromQml()
//{
//    qDebug()<<Q_FUNC_INFO;
//    emit sendToQml();
//}
