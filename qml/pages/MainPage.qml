/*******************************************************************************
**
** Copyright (C) 2022 io.github.danyok96
**
** This file is part of the Brainf*ck интерпретатор для Аврора ОС. project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: mainParent
    objectName: "mainPage"
    allowedOrientations: Orientation.All

    property int numOfCommas: 0
    function commasCounterAndOutputter(){

        for(var i = 0; i < inputTextArea.text.length; i++) {
            if(inputTextArea.text[i] === ",") {
                numOfCommas++
            }
        }
        console.log(numOfCommas)
        if(numOfCommas != 0) {
        var dialog = pageStack.push(Qt.resolvedUrl("StringInputDialog.qml"),
                                    {"number": numOfCommas.toString()})
        dialog.accepted.connect(function() {
            outputTextArea.text = brainFuck.interpret(inputTextArea.text, dialog.inputString)
        })
        } else {
        outputTextArea.text = brainFuck.interpret(inputTextArea.text, "")
        }
        numOfCommas = 0
    }
    PageHeader {
        id: header
        objectName: "pageHeader"
        title: qsTr("Bf4fun")
        extraContent.children: [
            IconButton {
                objectName: "aboutButton"
                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }
//    Connections{
//        target: brainFuck

//        onSendToQml: {
//            console.log("onSendToQml")
//        }
//    }
    TextArea {
        id: inputTextArea
        anchors.top: header.bottom
        height: parent.height * 0.5
        label: qsTr("Ваш Brainf*ck код")
    }
    Rectangle {
        id: buttonArea
        height: parent.height * 0.08
        anchors.left: inputTextArea.left
        anchors.top: inputTextArea.bottom
        Button {
            id: runButton
            text: qsTr("V Запустить V")
            onClicked: {
                commasCounterAndOutputter()
            }
        }
        Button {
            text: qsTr("Очистить")
            anchors.left: runButton.right
            onClicked: {
                inputTextArea.text = ""
                outputTextArea.text = ""
            }
        }
    }
    TextArea {
        id: outputTextArea
        anchors.top: buttonArea.bottom
        label: qsTr("Поле вывода")
        readOnly: true
    }
}
