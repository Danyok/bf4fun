import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    property string inputString
    property string number

    Column {
        width: parent.width

        DialogHeader {
        }

        TextField {
            id: inputStringField
            width: parent.width
            placeholderText: qsTr("Нужно ввести символов:")+number
            label: qsTr("Символы")
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            inputString = inputStringField.text.substring(0, number)
        }
    }
}
