<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="67"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Brainf*ck interpret for Aurora OS.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="72"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="46"/>
        <source>Bf4fun
by Danyok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="69"/>
        <source>Bf4fun</source>
        <translation type="unfinished">Bf4fun</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="91"/>
        <source>Ваш Brainf*ck код</source>
        <translation>Your Brainf*ck code</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="100"/>
        <source>V Запустить V</source>
        <translation>V Run V</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>Поле вывода</source>
        <translation>Output field</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="106"/>
        <source>Очистить</source>
        <translation>Clear</translation>
    </message>
</context>
<context>
    <name>StringInputDialog</name>
    <message>
        <location filename="../qml/pages/StringInputDialog.qml" line="17"/>
        <source>Нужно ввести символов:</source>
        <translation>You need to enter characters:</translation>
    </message>
    <message>
        <location filename="../qml/pages/StringInputDialog.qml" line="18"/>
        <source>Символы</source>
        <translation>Symbols</translation>
    </message>
</context>
</TS>
