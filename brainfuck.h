#ifndef BRAINFUCK_H
#define BRAINFUCK_H

#include <QObject>
#include <QDebug>

#define null NULL
#define true 1

#define BRAINFUCKS	65532

#define TK_COUNT	8
#define TK_NEXT		'>'
#define	TK_PREV		'<'
#define	TK_INC		'+'
#define	TK_DEC		'-'
#define	TK_OUT		'.'
#define TK_IN		','
#define	TK_STR		'['
#define	TK_END		']'

typedef struct _stack stack_t, *stack_p;
struct _stack {
    int					command;
    stack_p				next;
};

typedef struct _brainfuck {
    int					ptr;
    unsigned short int	data[BRAINFUCKS];
    int					commands;
    int					pos;
    char				code[BRAINFUCKS];
    stack_t *			stack;
} brainfuck_t;

class Brainfuck : public QObject
{
    Q_OBJECT
public:
    explicit Brainfuck(QObject *parent = nullptr);

    Q_INVOKABLE QString interpret(QString str, QString commasStr);

    QString brainfuck(QString inputStr, QString commasStr);

//signals:
//    void sendToQml();

//public slots:
//    void reciveFromQml();
};

#endif // BRAINFUCK_H
